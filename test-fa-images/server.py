import pathlib
from datetime import datetime
from fastapi import FastAPI, File, UploadFile, Depends, HTTPException, status
import aiofiles
import uvicorn


IMAGE_FOLDER_NAME: str = 'images'
FILES_CONTENT_TYPES: set = {'image/png', 'image/jpeg'}
CURRENT_FOLDER: pathlib.Path = pathlib.Path(__file__).parent.resolve()
IMAGE_FOLDER: pathlib.Path = pathlib.Path(CURRENT_FOLDER) / IMAGE_FOLDER_NAME

app: FastAPI = FastAPI()


@app.on_event('startup')
def prep_image_folder():
    if IMAGE_FOLDER.exists() and IMAGE_FOLDER.is_dir():
        return
    IMAGE_FOLDER.mkdir()


# в целом и такой вариант рабочий, но тогда необходимо подгружать весь файл в память
# async def async_save_file(content, filename, mode='wb'):
#     async with aiofiles.open(f'{image_folder}/{filename}', mode=mode) as f:
#         await f.write(content)


async def chek_file_types(file: UploadFile = File(...)) -> None:
    if not file.content_type or file.content_type not in FILES_CONTENT_TYPES:
        raise HTTPException(
          status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
          detail='Unsuported file type'
        )


async def check_file_name(file: UploadFile = File(...)) -> None:
    if not file.filename:
        raise HTTPException(
            status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
            detail='Invalid filename'
        )


def generate_filename(file: UploadFile = File(...)) -> str:
    now = datetime.utcnow().strftime("%Y_%m_%d-%I_%M_%S_%p")
    return f'{now}_{file.filename}'


def generate_filepath(filename=Depends(generate_filename)) -> pathlib.Path:
    return IMAGE_FOLDER / filename


@app.post('/images', dependencies=[Depends(chek_file_types), Depends(check_file_name)])
async def load_image(file: UploadFile = File(...), filepath=Depends(generate_filepath)):
    async with aiofiles.open(filepath, 'wb') as saved_file:  # chunked async reading and saving
        content = await file.read(1024)
        while content:
            await saved_file.write(content)
            content = await file.read(1024)
    return {'status': 'Ok'}


if __name__ == '__main__':
    uvicorn.run('server:app', host='127.0.0.1', port=8000, debug=True, reload=True)
