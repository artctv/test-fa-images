from typing import Generator, Any
import time
import pathlib
import asyncio
import aiofiles
import httpx


MODE: str = 'ASYNC'  # SYNC/ASYNC
CURRENT_FOLDER: pathlib.Path = pathlib.Path(__file__).parent.resolve()
IMAGE_FOLDER: str = 'image_folder'
FILE_TYPES: set = {".png", ".jpeg", ".jpg"}
URL: str = 'http://127.0.0.1:8000/images'
BATCH_SIZE: int = 4  # чисто для примера, вообще 100-200
SENDING_TIMEOUT: float = 0.5


def get_image_folder() -> pathlib.Path:
    image_folder: pathlib.Path = pathlib.Path(CURRENT_FOLDER) / IMAGE_FOLDER
    if not pathlib.Path(image_folder).is_dir():
        raise AttributeError('Required folder')
    return image_folder


def get_images(folder: pathlib.Path) -> Generator[pathlib.Path, Any, None]:
    files = (p.resolve() for p in pathlib.Path(folder).glob("**/*") if p.suffix in FILE_TYPES)
    return files


def prep_images() -> Generator[pathlib.Path, Any, None]:
    image_folder: pathlib.Path = get_image_folder()
    images = get_images(image_folder)
    return images


def sync_send_file(file: pathlib.Path) -> None:
    files = {'file': (file.name, file.open('rb'))}
    r = httpx.post(URL, files=files)
    print(r, r.text, r.status_code)


def sync_send_batch(batch_images: list[pathlib.Path]):
    for image in batch_images:
        sync_send_file(image)


def sync_load():
    images = prep_images()
    tasks = []
    for i, image in enumerate(images):
        tasks.append(image)
        if i % BATCH_SIZE == 0:
            sync_send_batch(tasks)
            time.sleep(SENDING_TIMEOUT)
            tasks.clear()
    else:
        if tasks:
            sync_send_batch(tasks)


async def async_send_task(image: pathlib.Path):
    async with aiofiles.open(image, 'rb') as f, httpx.AsyncClient() as client:
        file = await f.read()
        files = {'file': (image.name, file)}
        r = await client.post(URL, files=files)
        print(r, r.text, r.status_code)


async def async_load():
    images = prep_images()
    tasks = []
    for i, image in enumerate(images):
        tasks.append(asyncio.create_task(async_send_task(image)))
        if i % BATCH_SIZE == 0:
            await asyncio.gather(*tasks)
            # await asyncio.sleep(0.5)
            time.sleep(SENDING_TIMEOUT)  # блокируем луп для ожидания между партиями
            tasks.clear()
    else:
        if tasks:
            await asyncio.gather(*tasks)


if __name__ == '__main__':
    if MODE == 'SYNC':
        sync_load()
    elif MODE == 'ASYNC':
        asyncio.run(async_load())


